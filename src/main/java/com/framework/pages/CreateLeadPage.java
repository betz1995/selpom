package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	}

	
		
		//enter company name
		@FindBy(how = How.ID,using="createLeadForm_companyName")WebElement eleCompanyName;
		public CreateLeadPage enterCompanyName(String data) {
			clearAndType(eleCompanyName, data);
			return this;
			
			
		}
		//enter first name
		@FindBy(how = How.ID,using="createLeadForm_firstName")WebElement eleFirstname;
		public CreateLeadPage enterFirstname(String data) {
			clearAndType(eleFirstname, data);
			return this;
		}
		//enter last name
		@FindBy(how = How.ID,using="createLeadForm_lastName")WebElement eleLastname;
		public CreateLeadPage enterLastName(String data) {
			clearAndType(eleLastname, data);
			return this;
			
			
			
		}
		//click createLead
		@FindBy(how = How.CLASS_NAME,using = "smallSubmit") WebElement eleCreateLead;
		public ViewLeadPage clickCreateLead() {
		    click(eleCreateLead);
		    return new ViewLeadPage();
		}

}


















