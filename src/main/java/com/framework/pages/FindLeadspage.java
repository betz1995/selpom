package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadspage extends ProjectMethods{

	public FindLeadspage() {
       PageFactory.initElements(driver, this);
	}

	
		
		
			
			
		
		//enter first name
		@FindBy(how = How.NAME,using="firstName")WebElement eleFirstname2;
		public FindLeadspage enterFirstname(String data) {
			clearAndType(eleFirstname2, data);
			return this;
		}
		
		//enter last name
		@FindBy(how = How.NAME,using="lastName")WebElement eleLastname2;
		public FindLeadspage enterLastname(String data) {
			clearAndType(eleFirstname2, data);
			return this;
		}
		
		
		//click Find Leads
		@FindBy(how = How.XPATH,using = "//button[text()='Find Leads']") WebElement eleFindLeads;
		public ViewLeadPage clickFindLeads() {
		    click(eleFindLeads);
		    return new ViewLeadPage();
		}

}


















