package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	}

	//Verify lead creation
		
		//get the first name
		@FindBy(how = How.ID,using="viewLead_firstName_sp")WebElement eleFirstname1;
		public ViewLeadPage verifyFirstname(String data) {
			verifyExactText(eleFirstname1, data);
			return this;
		}
			
			//get the last name 
			@FindBy(how = How.ID,using="viewLead_lastName_sp")WebElement eleLastname1;
			public ViewLeadPage verifyLastname(String data) {
			verifyExactText(eleLastname1, data);
				/*if((txtFirstName.equals("Veena")) && (txtLastname.equals("Nian")) )
						{ 
					System.out.println("Lead is created successfully");
						}
				else
					System.out.println("Lead is not created");*/
				return this;
				
				
			
			
		}

}


















