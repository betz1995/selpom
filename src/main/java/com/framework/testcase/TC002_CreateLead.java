package com.framework.testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadPage;
import com.framework.pages.LoginPage;
import com.framework.pages.CreateLeadPage;

public class TC002_CreateLead extends ProjectMethods {
@BeforeTest
public void setData() {
testCaseName = "TC002_CreateLead";
testDescription = "Create Lead";
testNodes = "Leads";
author = "Betty";
category = "smoke";
dataSheetName = "TC002";
	
}

@Test(dataProvider="fetchData")
public void login(String username,String password,String companyname,String firstname,String lastname) {
	new LoginPage()
	.enterUsername(username)
	.enterPassword(password)
	.clickLogin()
	.clickCrmsfa()
	.clickLeads()
	.CreateLeadLink()
	.enterCompanyName(companyname)
	.enterFirstname(firstname)
	.enterLastName(lastname)
	.clickCreateLead()
	.verifyFirstname(firstname)
	.verifyLastname(lastname);
	 

	
}
}
