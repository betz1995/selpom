package com.framework.testcase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {
@BeforeTest
public void setData() {
testCaseName = "TC001_LoginLogout";
testDescription = "Login into test leaf";
testNodes = "Leads";
author = "Betty";
category = "smoke";
dataSheetName = "TC001";

}
@Test(dataProvider="fetchData")
public void login(String username,String password) {
	new LoginPage()
	.enterUsername(username)
	.enterPassword(password)
	.clickLogin()
	.clickLogout();
	
	
	
}
}
